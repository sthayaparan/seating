package simulation

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	BufferSize     = 5000
	NoOfPassengers = 100
	NoOfSeats      = 100
)

var (
	semaphore = make(chan struct{}, BufferSize)
	completed = make(chan bool)
)

type Counter struct {
	mutex     sync.Mutex
	stats     map[bool]int
	processed int
}

func (c *Counter) update(key bool, runs int) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.stats[key]++
	c.processed++

	if c.processed == runs {
		completed <- true
	}
}

func (c *Counter) calcProbability() float64 {

	total := c.stats[true] + c.stats[false]
	lastTaken := c.stats[true]

	return float64(lastTaken) / float64(total)
}

var c = Counter{stats: make(map[bool]int)}

func runItem(passengers, seats, runs int) {
	taken := seatTaken(passengers, seats)
	c.update(taken, runs)
}

func Run(runs int) float64 {

	fmt.Printf("Simulation started for %v runs\n", runs)

	for i := 0; i < runs; i++ {

		semaphore <- struct{}{}
		go func() {
			runItem(NoOfPassengers, NoOfSeats, runs)
			<-semaphore
		}()
	}
	<-completed

	return c.calcProbability()
}

func seatTaken(passengers, seats int) bool {

	seatPassengerMap := make(map[int]int, passengers)
	rand.Seed(time.Now().UnixNano())

	for i := 1; i < passengers+1; i++ {
		if _, taken := seatPassengerMap[i]; i == 1 || taken {
			randomSeatNum := rand.Intn(passengers) + 1
			_, taken := seatPassengerMap[randomSeatNum]
			for taken {
				randomSeatNum = rand.Intn(passengers) + 1
				_, taken = seatPassengerMap[randomSeatNum]
			}
			seatPassengerMap[randomSeatNum] = i
		} else {
			seatPassengerMap[i] = i
		}

		if passenger, taken := seatPassengerMap[passengers]; taken && passenger != passengers {
			return false
		}
	}
	return true
}
