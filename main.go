package main

import (
	"fmt"
	"seating/simulation"
)

var noOfRuns = 10000

func main() {
	probability := simulation.Run(noOfRuns)
	fmt.Printf("Probability %v", probability)
}
